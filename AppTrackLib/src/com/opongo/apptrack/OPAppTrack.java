package com.opongo.apptrack;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

public class OPAppTrack {
	private final static String TAG = OPAppTrack.class.getSimpleName();
	public final static OPSettings opSettings = new OPSettings();
	
	public static void startSessionWithToken(final Context context, final String token, final Boolean debug) {
		opSettings.DEBUG = debug;
		
		SharedPreferences settings = context.getSharedPreferences(OPSettings.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		
		long lastServerDate = settings.getLong("lastServerDate", 0);
		long now = new Date().getTime();
		
		int opaDelay = settings.getInt("opa_delay", 0);
				
		if (lastServerDate < (now - 1000 * opaDelay)) {
			editor.putLong("lastServerDate", now);
			editor.commit();
			
			OPAppTrack.startSession(context, token, OPSettings.kOPStartDelay);
			
		}
	}
	
	public static void startSession(final Context context, final String token, final double delay) {
		final Handler handler = new Handler();
		
		handler.postDelayed(new Runnable() {
			  @Override
			  public void run() {
					SessionRequestAsyncTask sessionRequestAsyncTask = new SessionRequestAsyncTask();
					sessionRequestAsyncTask.context = context;
					sessionRequestAsyncTask.execute(token);
			  }
		}, (long)delay*1000);
	}

	public static class SessionRequestAsyncTask extends AsyncTask<String, Void, String> {		
		private Context context;
		private SharedPreferences settings;
		private SharedPreferences.Editor editor;
		private String token;
		
		@Override
		protected String doInBackground(String... params) {	
			this.token = params[0];
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			nameValuePairs.add(new BasicNameValuePair("token", token));
			
			nameValuePairs.add(new BasicNameValuePair("device_name", Build.MODEL));
			nameValuePairs.add(new BasicNameValuePair("device_type", Build.DEVICE));
			nameValuePairs.add(new BasicNameValuePair("device_version", String
					.valueOf(Build.VERSION.SDK_INT)));
			
			Info adInfo = null;
			String device_id = null;
			boolean isLAT = false;
			
			try {
				adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
			  
			  	device_id = adInfo.getId();
				isLAT = adInfo.isLimitAdTrackingEnabled();
				
			} catch (IOException e) {
			  // Unrecoverable error connecting to Google Play services (e.g.,
			  // the old version of the service doesn't support getting AdvertisingId).
			  if (opSettings.DEBUG) {
				Log.e(TAG, "ClientProtocolException in IOException");
				e.printStackTrace();
			  }
			} catch (GooglePlayServicesNotAvailableException e) {
			  // Google Play services is not available entirely.
		      if (opSettings.DEBUG) {
				Log.e(TAG, "ClientProtocolException in GooglePlayServicesNotAvailableException");
				e.printStackTrace();
			  }
			} catch (IllegalStateException e) {
			  // Google Play services is not available entirely.
			  if (opSettings.DEBUG) {
				Log.e(TAG, "ClientProtocolException in IllegalStateException");
				e.printStackTrace();
			  }
			} catch (GooglePlayServicesRepairableException e) {
			  // Google Play services is not available entirely.
			  if (opSettings.DEBUG) {
				Log.e(TAG, "ClientProtocolException in GooglePlayServicesRepairableException");
				e.printStackTrace();
			  }
			} 
			catch (NullPointerException e) {
				// handle exception
				  if (opSettings.DEBUG) {
					Log.e(TAG, "NullPointerException in Exception");
					e.printStackTrace();
				  }
			} catch (Exception e) {
				// handle exception
				  if (opSettings.DEBUG) {
					Log.e(TAG, "ClientProtocolException in Exception");
					e.printStackTrace();
				  }
			}
			  
			if(isLAT || device_id == null) return null;
				
			nameValuePairs.add(new BasicNameValuePair("advertising_identifier", device_id));
			
			String locale = context.getResources().getConfiguration().locale.getLanguage();
			nameValuePairs.add(new BasicNameValuePair("language", locale));
			
			try {
				URL url = new URL(opSettings.DEBUG?OPSettings.kOPDebugServerURL:OPSettings.kOPServerURL);
				
				String result = OPHttpConnect.sendRequest(url, nameValuePairs);
				
				return result;
				
			} catch (MalformedURLException e) {
				if (opSettings.DEBUG) {
					Log.e(TAG, "MalformedURLException in SessionRequestAsyncTask");
					e.printStackTrace();
				}
			} catch (ClientProtocolException e) {
				if (opSettings.DEBUG) {
					Log.e(TAG, "ClientProtocolException in SessionRequestAsyncTask");
					e.printStackTrace();
				}
			} catch (IOException e) {
				if (opSettings.DEBUG) {
					Log.e(TAG, "IOException in SessionRequestAsyncTask");
					e.printStackTrace();
				}
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (opSettings.DEBUG) Log.d(TAG, "onPostExecute");
			
			this.settings = this.context.getSharedPreferences(OPSettings.PREFS_NAME, 0);
			this.editor = settings.edit();
			
			if(result != null) {
				int delay = 0;
				
				try {
					if(!result.equals("")) {
						JSONObject obj = new JSONObject(result);
						delay = obj.getInt("delay");
					}
					
				} catch (JSONException e) {
					if (opSettings.DEBUG) {
						Log.e(TAG, "JSONException in SessionRequestAsyncTask.onPostExecute");
						e.printStackTrace();
					}
				}
				
				this.editor.putInt("opa_delay", delay);
				this.editor.commit();
			} else {
				OPAppTrack.startSession(this.context, this.token, OPSettings.kOPRetryDelay);
			}
		}		
	}
}
