package com.opongo.apptrack;

public class OPSettings {
	public final static String kOPServerURL = "https://analytics.opongo.com/api";
	public final static String kOPDebugServerURL = "https://analytics.opongo.com/api/debug";
	public final static String PREFS_NAME = "OPAnalytics";
	
	public final static Double kOPStartDelay =  1.5;
	public final static Double kOPRetryDelay = 10.0;
	
	public Boolean DEBUG = false;
}
